
void print_help_common(void);
void reject_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref, uint8_t isdn_cause);
void release_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref, uint8_t isdn_cause);
void disconnect_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref, uint8_t isdn_cause);
void proceed_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref, const char *sdp);
void alert_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref);
void answer_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref);
void setup_comp_call(osmo_cc_endpoint_t *cc_ep, uint32_t callref);
int generate_dial_string(uint8_t type, const char *dialing, char *string, int string_size);
int parse_dial_string(uint8_t *type, char *dialing, int dialing_size, const char *string);

