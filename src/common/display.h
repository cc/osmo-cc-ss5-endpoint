
#define MAX_DISPLAY_WIDTH 1024
#define MAX_HEIGHT_STATUS 64

void display_status_on(int on);
void display_status_start(void);
void display_status_line(const char *if_name, int link_count, const char *from_id, const char *to_id, const char *state_name);
void display_status_end(void);

