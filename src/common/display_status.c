/* display status functions
 *
 * (C) 2020 by Andreas Eversberg <jolly@eversberg.eu>
 * All Rights Reserved
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include "../liblogging/logging.h"
#include "display.h"

static int status_on = 0;
static int line_count = 0;
static int lines_total = 0;
static char screen[MAX_HEIGHT_STATUS][MAX_DISPLAY_WIDTH];
static char screen_color[MAX_HEIGHT_STATUS][MAX_DISPLAY_WIDTH];

static void print_status(int on)
{
	int i, j;
	int w, h;
	char color, last_color = -1;

	get_win_size(&w, &h);
	if (w > MAX_DISPLAY_WIDTH - 1)
		w = MAX_DISPLAY_WIDTH - 1;

	if (w > MAX_DISPLAY_WIDTH)
		w = MAX_DISPLAY_WIDTH;
	h--;
	if (h > lines_total)
		h = lines_total;

	lock_logging();
	enable_limit_scroll(false);
	printf("\0337\033[H");
	for (i = 0; i < h; i++) {
		j = 0;
		if (on) {
			for (j = 0; j < w; j++) {
				color = screen_color[i][j];
				if (screen[i][j] > ' ' && last_color != color) {
					printf("\033[%d;3%dm", color / 10, color % 10);
					last_color = color;
				}
				putchar(screen[i][j]);
			}
		} else {
			for (j = 0; j < w; j++)
				putchar(' ');
		}
		putchar('\n');
	}
	printf("\033[0;39m\0338"); fflush(stdout);
	enable_limit_scroll(true);
	unlock_logging();
}

void display_status_on(int on)
{
	if (status_on)
		print_status(0);

	if (on < 0)
		status_on = 1 - status_on;
	else
		status_on = on;

	if (status_on)
		print_status(1);

	if (status_on)
		logging_limit_scroll_top(lines_total);
	else
		logging_limit_scroll_top(0);
}

/* start status display */
void display_status_start(void)
{
	memset(screen, ' ', sizeof(screen));
	memset(screen_color, 7, sizeof(screen_color));
	memset(screen[0], '-', sizeof(screen[0]));
	memcpy(screen[0] + 4, "Call Status", 11);
	line_count = 1;
}

const char *display_digits(const char *digits)
{
	static char display[256];
	int i, j;

	if (*digits == '\0')
		return "";

	for (i = 0, j = 0; digits[i]; i++) {
		/* We need minimum of 5 digits space: e.g. '-C11\0' */
		if (j + 5 > (int)sizeof(display))
			break;
		/* if this is not the first digits && this is special digit or previous digit was special digit */
		if (i && ((digits[i - 1] >= 'a' && digits[i - 1] <= 'e') || (digits[i] >= 'a' && digits[i] <= 'e')))
			display[j++] = '-';
		switch (digits[i]) {
		case 'a':
			display[j++] = 'K';
			display[j++] = 'P';
			display[j++] = '1';
			break;
		case 'b':
			display[j++] = 'K';
			display[j++] = 'P';
			display[j++] = '2';
			break;
		case 'c':
			display[j++] = 'S';
			display[j++] = 'T';
			break;
		case 'd':
			display[j++] = 'C';
			display[j++] = '1';
			display[j++] = '1';
			break;
		case 'e':
			display[j++] = 'C';
			display[j++] = '1';
			display[j++] = '2';
			break;
		default:
			display[j++] = digits[i];
		}
	}

	display[j] = '\0';
	return display;
}

void display_status_line(const char *if_name, int link_count, const char *from_id, const char *to_id, const char *state_name)
{
	char line[MAX_DISPLAY_WIDTH + 4096];
	char color[MAX_DISPLAY_WIDTH + 4096];
	int index, from_len;

	memset(color, 7, sizeof(color)); // make valgrind happy

	if (line_count == MAX_HEIGHT_STATUS)
		return;

	/* at first interface or when it changes */
	if (!link_count)
                line_count++;

	/* check line count again */
	if (line_count == MAX_HEIGHT_STATUS)
		return;

	if (!link_count) {
		/* interface */
		strcpy(line, if_name);
		memset(color, 3, strlen(if_name));
	} else {
		memset(line, ' ', strlen(if_name));
	}
	index = strlen(if_name);

	if (from_id && to_id) {
		/*  '<id>' */
		sprintf(line + index, " \'%s\' -> \'%s\' (%s) ", from_id, to_id, display_digits(to_id));
		from_len = strlen(from_id);
		memset(color + index, 1, 4 + from_len);
		memset(color + index + 4 + from_len, 7, 2);
		memset(color + index + 4 + from_len + 2, 2, strlen(line + index) - 4 - from_len - 2);
		index += strlen(line + index);
		line[index] = '\0';

		strcpy(line + index, state_name);
		memset(color + index, 7, strlen(state_name));
	}

	/* store line without CR, but not more than MAX_DISPLAY_WIDTH - 1 */
	line[MAX_DISPLAY_WIDTH - 1] = '\0';
	memcpy(screen[line_count], line, strlen(line));
	memcpy(screen_color[line_count], color, strlen(line));
	line_count++;
}

void display_status_end(void)
{
	if (line_count < MAX_HEIGHT_STATUS)
		line_count++;

	if (line_count < MAX_HEIGHT_STATUS) {
		memset(screen[line_count], '-', sizeof(screen[line_count]));
		line_count++;
	}
	/* if last total lines exceed current line count, keep it, so removed lines are overwritten with spaces */
	if (line_count > lines_total)
		lines_total = line_count;
	if (status_on)
		print_status(1);
	/* set new total lines */
	lines_total = line_count;
	if (status_on)
		logging_limit_scroll_top(lines_total);
}

