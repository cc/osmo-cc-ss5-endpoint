
#include <osmocom/core/timer.h>
#include <osmocom/core/select.h>
#include <osmocom/cc/endpoint.h>
#include <osmocom/cc/helper.h>
#include "../libsample/sample.h"
#include "../libjitter/jitter.h"
#include "mf.h"

typedef struct dsp {
	struct dsp *next;		/* next instance (list is maintained by dsp) */
	void *priv;			/* back pointer to application */
	char name[32];			/* name of link for debugging */
	double samplerate;

	/* tone generation */
	mf_mod_t *mf_mod;		/* MF modulator */
	char tone;			/* current digit playing or 0 */
	int tone_transparent;		/* mix tone with audio */
	uint32_t tone_mask;		/* bit-mask of which MF tones to play */
	int tone_duration;		/* counter of samples for length of tone */
	char dial_string[33];		/* stores digits when dialing number sequence */
	int dial_length;		/* length of dial string, or 0 */
	int dial_index;			/* current position at dial string */
	int digit_on;			/* flag that states if digit is currently playing */
	double kp_digit_duration;	/* duration of kp digit */
	double other_digit_duration;	/* duration of other digits */
	double digit_pause;		/* interdigit pause between */
	int pulsedialing;		/* using pulses to dial */
	int pulse_on;			/* flag that states if pulse is currently on */
	int pulse_num;			/* number of pulses to send */
	int pulse_count;		/* counter for pulses in one digit */
	double pulse_pause;		/* interdigit pause between pulse digits */
	int sit_on, sit_count;		/* sit tone generator */

	/* tone detection */
	mf_demod_t *mf_demod;		/* MF demodulator */
	double ms_per_sample;		/* how many milliseconds between two samples */
	double ms_interval;		/* counts milliseconds */
	int ms_count;			/* counts samples within millisecond interval */
	int interrupt_recognition;	/* number of milliseconds until interruption is detected */
	int interrupt_count;		/* counter for interrupt condition */
	int split_recognition;		/* number of milliseconds until split (mute) condition meats */
	int split_count;		/* counter for split condition */
	int sig_detect_duration_AB;	/* signaling tone duration in milliseconds (TONE A/B) */
	int sig_detect_duration_C;	/* signaling tone duration in milliseconds (TONE C) */
	int mf_recognition;		/* MF tone duration in milliseconds */
	int detect_count;		/* counter for tone detection */
	char detect_tone;		/* current tone detected or ' ' for no tone */

	/* audio processing */
	jitter_t tx_dejitter;		/* jitter buffer for audio from CC */
	sample_t *delay_buffer;		/* buffer for delaying audio */
	int delay_length;
	int delay_index;
	int crosstalk;			/* mix crosstalk from TX to RX */
	int comfort_noise;		/* add comfort noise before answer and after disconnect */
	int notch;
	iir_filter_t notch_in_filter;	/* remove 2600 Hz from trunk */
	iir_filter_t notch_out_filter;	/* remove 2600 Hz to trunk */

	/* osmo-CC */
	uint32_t cc_callref;		/* ref to CC call */
	osmo_cc_session_t *cc_session;	/* audio session description */
	osmo_cc_session_codec_t *codec;	/* selected codec */
} dsp_t;

void dsp_set_sf(double sf_db, double sf_min_db);
int dsp_init_inst(dsp_t *dsp, void *priv, const char *name, double samplerate, int crosstalk, int comfort_noise, int delay_ms, double sense_db, double interrupt_recognition, double split_recognition, double mf_recognition, double kp_digit_duration, double other_digit_duration, double digit_pause, double pulse_pause, double notch, int ss5);
void dsp_cleanup_inst(dsp_t *dsp);
void set_sig_detect_duration(dsp_t *dsp, double duration_AB, double duration_C);
int set_tone(dsp_t *dsp, char tone, double duration);
void set_tone_transparent(dsp_t *dsp, int transparent);
void set_sit(dsp_t *dsp, int on);
void set_dial_string(dsp_t *dsp, const char *dial, int pulsedialing);
void audio_clock(dsp_t *sunset_list, dsp_t *sunrise_list, int len);
void down_audio(struct osmo_cc_session_codec *codec, uint8_t marker, uint16_t sequence, uint32_t timestamp, uint32_t ssrc, uint8_t *data, int len);
void encode_l16(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);
void decode_l16(uint8_t *src_data, int src_len, uint8_t **dst_data, int *dst_len, void *priv);

void receive_signal(void *priv, char digit, double dbm);
void dialing_complete(void *priv);

