
#include "../libfilter/iir_filter.h"

struct mf_mod_tone {
	double rot;
	double phase;
	double amplitude;
};

typedef struct mf_mod {
	int tones;
	struct mf_mod_tone tone[0];
} mf_mod_t;

struct mf_demod_tone {
	double rot;
	double phase;
	iir_filter_t lp[2];
};

typedef struct mf_demod {
	int tones;
	struct mf_demod_tone tone[0];
} mf_demod_t;

int mf_init(int _fast_math);
void mf_exit(void);
mf_mod_t *mf_mod_init(double samplerate, int tones, double *freq, double *amplitude);
void mf_mod_exit(mf_mod_t *mod);
void mf_mod(mf_mod_t *mod, uint32_t *mask, sample_t *samples, int length, int transparent);
mf_demod_t *mf_demod_init(double samplerate, int tones, double *freq, double *width);
void mf_demod_exit(mf_demod_t *demod);
void mf_demod(mf_demod_t *demod, sample_t *samples, int length, sample_t **levels_squared);

