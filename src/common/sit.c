
#include <stdlib.h>
#include <math.h>
#include "../libsample/sample.h"
#include "sit.h"

#define db2level(db)	pow(10, (double)(db) / 20.0)

#define SIT_F1		950.0
#define SIT_F2		1400.0
#define SIT_F3		1800.0
#define SIT_LEVEL	-24.0	/* -24 dBm */
#define SIT_DURATION	0.330	/* tone duration */
#define SIT_PAUSE	1.0	/* tone pause */

static sample_t *sit_samples;
static int sit_length = 0;

void init_sit(int samplerate)
{
	int length, i;
	double level = db2level(SIT_LEVEL);

	length = (int)((SIT_DURATION * 3 + SIT_PAUSE) * (double)samplerate);
	sit_samples = calloc(length, sizeof(*sit_samples));
	if (!sit_samples)
		abort();
	sit_length = length;

	length = (int)(SIT_DURATION * (double)samplerate);
	for (i = 0; i < length; i++)
		sit_samples[i] = sin(2.0 * M_PI * SIT_F1 * (double)i / (double)samplerate) * level;
	for (; i < length * 2; i++)
		sit_samples[i] = sin(2.0 * M_PI * SIT_F2 * (double)i / (double)samplerate) * level;
	for (; i < length * 3; i++)
		sit_samples[i] = sin(2.0 * M_PI * SIT_F3 * (double)i / (double)samplerate) * level;
}

void exit_sit(void)
{
	free(sit_samples);
	sit_length = 0;
}

int sit_play(sample_t *samples, int count, int length)
{
	while (length--) {
		*samples++ = sit_samples[count];
		if (++count == sit_length)
			count = 0;
	}
	return count;
}
