
enum {
	DLCC,
	DOPTIONS,
	DJITTER,
	DDSP,
	DSS5,
	DR1,
};

#define DLCC_DEFINED

extern struct log_info_cat log_categories[];
extern size_t log_categories_size;

