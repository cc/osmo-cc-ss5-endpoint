/* R1 process
 *
 * (C) 2023 by Andreas Eversberg <jolly@eversberg.eu>
 * All Rights Reserved
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CHAN r1->name

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include "../liblogging/logging.h"
#include <osmocom/cc/g711.h>
#include "r1.h"
#include "../common/display.h"
#include "../common/common.h"

/* names of all R1 states */
const char *r1_state_names[] = {
	"NULL",
	/* idle line */
	"IDLE",
        /* outgoing call */
	"SEIZING",
	"OUT DELAY-DIALING",
	"SEND DIGITS",
	"OUT PROCEED",
	"OUT ANSWER",
	"OUT HANGUP",
        /* incoming call */
	"IN DELAY-DIALING",
	"RECV DIGIT WAIT",
	"RECV DIGIT ON",
	"RECV PULSE WAIT",
	"RECV PULSE ON",
	"RECV PULSE OFF",
	"IN PROCEED",
	"IN ANSWER",
	"IN HANGUP",
};

/* timers and durations */
#define INTERRUPT_RECOGNITION   0.030		/* an interruption of 30 ms is considered to be a stop of digit */
#define SPLIT_RECOGNITION	0.015		/* time until received audio is split (band-stop filter turned on) */
#define MF_RECOGNITION          0.025
#define KP_DIGIT_DURATION	0.100
#define OTHER_DIGIT_DURATION	0.068
#define DIGIT_PAUSE		0.068
#define PULSE_PAUSE		0.600
#define SIGN_RECOGNITION_FAST	0.030		/* all tones except clear forward */
#define SIGN_RECOGNITION_SLOW	0.300		/* clear forward after providing a register */
#define HANGUP_TIMEOUT		0.013		/* timeout after receiving hangup, then disconnect */
#define CLEAR_FORWARD		0.300		/* tone on for at least 300ms to recognise as clear-forward */
#define TO_SEIZING		5,0		/* timeout waiting for a register (double seizure detection) */
#define TO_DELAY_DIALING	0,140000	/* time to wait until dialing register is available */
#define TO_DIALING		5,0		/* timeout when receiving no additional digits */
#define TO_PROCEEDING		60,0		/* timeout when there is no answer */
#define TO_PULSE		0,300		/* after this time the pulse is too long or states a new digit */
#define TO_GUARD_BUSY		1,250		/* guard time to wait until outgoing calls are allowed */

static struct osmo_cc_helper_audio_codecs codecs[] = {
	{ "L16", 8000, 1, encode_l16, decode_l16 },
	{ "PCMA", 8000, 1, g711_encode_alaw, g711_decode_alaw },
	{ "PCMU", 8000, 1, g711_encode_ulaw, g711_decode_ulaw },
	{ NULL, 0, 0, NULL, NULL},
};

void refresh_status(void)
{
	osmo_cc_endpoint_t *ep;
	r1_endpoint_t *r1_ep;
	dsp_t *dsp;
	r1_t *r1;
	int i;

	display_status_start();

	for (ep = osmo_cc_endpoint_list; ep; ep = ep->next) {
		r1_ep = ep->priv;
		if (!r1_ep->dsp_list)
			display_status_line(ep->local_name, 0, NULL, NULL, "");
		for (i = 0, dsp = r1_ep->dsp_list; dsp; i++, dsp = dsp->next) {
			r1 = dsp->priv;
			display_status_line(ep->local_name, i, r1->callerid, r1->dialing, r1_state_names[r1->state]);
		}
	}

	display_status_end();
}

#define r1_new_state(r1, state) _r1_new_state(r1, state, __LINE__)
static void _r1_new_state(r1_t *r1, enum r1_state state, int line)
{
	LOGP_CHAN(DR1, LOGL_DEBUG, "Changing state  '%s' -> '%s' (called from line %d)\n", r1_state_names[r1->state], r1_state_names[state], line);
	r1->state = state;

	/* must update (new state) */
	refresh_status();
}

/*
 * endpoints & links
 */

/* reset R1 link, but keep states, so audio generation/processing can continue */
static void link_reset(r1_t *r1)
{
	/* unlink callref */
	r1->dsp.cc_callref = 0;

	/* stop timer */
	osmo_timer_del(&r1->timer);

	/* free session description */
	if (r1->dsp.cc_session) {
		osmo_cc_free_session(r1->dsp.cc_session);
		r1->dsp.cc_session = NULL;
		r1->dsp.codec = NULL;
	}

	/* reset jitter buffer */
	jitter_reset(&r1->dsp.tx_dejitter);

	/* set recognition time */
	set_sig_detect_duration(&r1->dsp, SIGN_RECOGNITION_FAST, 0.0);

	/* make tone gerator non-transparent */
	set_tone_transparent(&r1->dsp, 0);

	/* reset all other states */
	r1->callerid[0] = '\0';
	r1->dialing[0] = '\0';

	/* must update (e.g. caller and dialing) */
	refresh_status();
}

static void r1_timeout(void *data);

static r1_t *link_create(r1_endpoint_t *r1_ep, const char *ep_name, int linkid, double samplerate, int crosstalk, int delay_ms, double sense_db, int pulsedialing)
{
	r1_t *r1;
	dsp_t **dsp_p;

	r1 = calloc(1, sizeof(*r1));
	if (!r1) {
		LOGP(DR1, LOGL_ERROR, "No memory!\n");
		abort();
	}
	r1->r1_ep = r1_ep;
	r1->pulsedialing = pulsedialing;

	/* debug name */
	snprintf(r1->name, sizeof(r1->name) - 1, "%s/%d", ep_name, linkid);

	/* init dsp instance */
	dsp_init_inst(&r1->dsp, r1, r1->name, samplerate, crosstalk, 0, delay_ms, sense_db, INTERRUPT_RECOGNITION, SPLIT_RECOGNITION, MF_RECOGNITION, KP_DIGIT_DURATION, OTHER_DIGIT_DURATION, DIGIT_PAUSE, PULSE_PAUSE, 2600.0, 0);

	/* init timer */
	osmo_timer_setup(&r1->timer, r1_timeout, r1);

	/* reset instance */
	link_reset(r1);

	/* Turn on SF */
	set_tone(&r1->dsp, 'B', 0);

	/* link */
	dsp_p = &r1_ep->dsp_list;
	while (*dsp_p)
		dsp_p = &((*dsp_p)->next);
	*dsp_p = &r1->dsp;

	LOGP_CHAN(DR1, LOGL_DEBUG, "created R1 instance\n");

	return r1;
}

static void link_destroy(r1_t *r1)
{
	dsp_t **dsp_p;

	/* detach */
	dsp_p = &r1->r1_ep->dsp_list;
	while (*dsp_p) {
		if (*dsp_p == &r1->dsp)
			break;
		dsp_p = &((*dsp_p)->next);
	}
	*dsp_p = r1->dsp.next;

	/* state idle */
	r1_new_state(r1, R1_STATE_NULL);

	/* reset instance */
	link_reset(r1);

	/* exit timer */
	osmo_timer_del(&r1->timer);

	/* cleanup dsp instance */
	dsp_cleanup_inst(&r1->dsp);

	LOGP_CHAN(DR1, LOGL_DEBUG, "destroyed R1 instance\n");

	free(r1);
}

r1_endpoint_t *r1_ep_create(const char *ep_name, int links, int suppress_disconnect, int clear_back_timeout, int crosstalk, int delay_ms, double sense_db, int no_l16, int pulsedialing)
{
	r1_endpoint_t *r1_ep;
	int i;

	r1_ep = calloc(1, sizeof(*r1_ep));
	if (!r1_ep) {
		LOGP(DR1, LOGL_ERROR, "No memory!\n");
		abort();
	}

	r1_ep->suppress_disconnect = suppress_disconnect;
	r1_ep->clear_back_timeout = clear_back_timeout;
	r1_ep->no_l16 = !!no_l16;

	for (i = 0; i < links; i++)
		link_create(r1_ep, ep_name, i + 1, 8000.0, crosstalk, delay_ms, sense_db, pulsedialing);

	LOGP(DR1, LOGL_DEBUG, "R1 endpoint instance created\n");

	return r1_ep;
}

void r1_ep_destroy(r1_endpoint_t *r1_ep)
{
	/* destroy all calls */
	while (r1_ep->dsp_list)
		link_destroy(r1_ep->dsp_list->priv);

	free(r1_ep);

	LOGP(DR1, LOGL_DEBUG, "R1 endpoint instance destroyed\n");
}

/*
 * event handling
 */

static void setup_call(r1_t *r1)
{
	osmo_cc_msg_t *msg;
	uint8_t type;
	char dialing[sizeof(r1->dialing)];
	int rc;

	LOGP_CHAN(DR1, LOGL_INFO, "Dialing '%s' is complete, sending setup message towards call control.\n", r1->dialing);
	/* stop timer */
	osmo_timer_del(&r1->timer);
	/* check and convert dial string, only do that for MF dialing */
	if (r1->pulsedialing) {
		type = OSMO_CC_TYPE_UNKNOWN;
		strcpy(dialing, r1->dialing);
	} else {
		rc = parse_dial_string(&type, dialing, sizeof(dialing), r1->dialing);
		if (rc < 0) {
			/* state hangup */
			r1_new_state(r1, R1_STATE_IN_HANGUP);
			/* send SIT */
			set_sit(&r1->dsp, 1);
			return;
		}
	}
	/* setup message */
	msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_IND);
	/* network type */
	osmo_cc_add_ie_calling_network(msg, OSMO_CC_NETWORK_R1_NONE, "");
	/* called number */
	osmo_cc_add_ie_called(msg, type, OSMO_CC_PLAN_TELEPHONY, dialing);
	/* sdp offer */
	r1->dsp.cc_session = osmo_cc_helper_audio_offer(&r1->r1_ep->cc_ep.session_config, &r1->dsp, codecs + r1->r1_ep->no_l16, down_audio, msg, 1);
	if (!r1->dsp.cc_session) {
		osmo_cc_free_msg(msg);
		LOGP_CHAN(DR1, LOGL_NOTICE, "Failed to offer audio, sending 'clear-back'.\n");
		/* state hangup */
		r1_new_state(r1, R1_STATE_IN_HANGUP);
		/* send SIT */
		set_sit(&r1->dsp, 1);
		return;
	}
	/* create new call */
	osmo_cc_call_t *cc_call = osmo_cc_call_new(&r1->r1_ep->cc_ep);
	r1->dsp.cc_callref = cc_call->callref;
	/* send message to CC */
	osmo_cc_ll_msg(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, msg);
	/* change state */
	r1_new_state(r1, R1_STATE_IN_PROCEED);
}

/* function that receives the signal or the cease of it (' ' or different signal) */
void receive_signal(void *priv, char signal, double dbm)
{
	r1_t *r1 = priv;
	const char *state_name = r1_state_names[r1->state];
	int i;

	if (signal > ' ')
		LOGP_CHAN(DR1, LOGL_DEBUG, "Received signal '%c' in '%s' state. (%.1f dBm)\n", signal, state_name, dbm);
	else
		LOGP_CHAN(DR1, LOGL_DEBUG, "Received cease of signal in '%s' state.\n", state_name);

	switch (r1->state) {
	/* initial SF when link is connected */
	case R1_STATE_NULL:
		if (signal == 'B') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'Idle' signal in '%s' state, line is connected, going idle.\n", state_name);
			/* state idle */
			r1_new_state(r1, R1_STATE_IDLE);
		}
		break;
	/* outgoing call sends seize */
	case R1_STATE_SEIZING:
		if (signal != 'B') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'Delay-Dialing' signal in '%s' state, waiting for dial register..\n", state_name);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_SEIZING);
			/* change state */
			r1_new_state(r1, R1_STATE_OUT_DELAY_DIALING);
		}
		break;
	/* outgoing call receives delay-dialing */
	case R1_STATE_OUT_DELAY_DIALING:
		if (signal == 'B') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'proceed-to-send' signal in '%s' state, send digits.\n", state_name);
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* dial */
			set_dial_string(&r1->dsp, r1->dialing, r1->pulsedialing);
			/* change state */
			r1_new_state(r1, R1_STATE_SEND_DIGITS);
		}
		break;
	/* outgoing call receives answer */
	case R1_STATE_SEND_DIGITS:
	case R1_STATE_OUT_PROCEED:
		if (signal == ' ') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'answer' signal in '%s' state.\n", state_name);
			/* change state */
			r1_new_state(r1, R1_STATE_OUT_ANSWER);
			/* indicate answer to upper layer */
			answer_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref);
		}
		break;
	/* outgoing call receives clear-back */
	case R1_STATE_OUT_ANSWER:
		if (signal == 'B') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'clear-back' signal in '%s' state'.\n", state_name);
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* start timer */
			if (r1->r1_ep->clear_back_timeout)
				osmo_timer_schedule(&r1->timer, r1->r1_ep->clear_back_timeout,0);
			/* change state */
			r1_new_state(r1, R1_STATE_OUT_HANGUP);
			if (!r1->r1_ep->suppress_disconnect) {
				/* indicate disconnect w/tones to upper layer */
				disconnect_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
			}
		}
		break;
	/* outgoing call receives answer after clear-back */
	case R1_STATE_OUT_HANGUP:
		if (signal == ' ') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'answer' signal in '%s' state.\n", state_name);
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* change state */
			r1_new_state(r1, R1_STATE_OUT_ANSWER);
		}
		break;
	/* incoming call receives seize */
	case R1_STATE_IDLE:
		if (signal == ' ') {
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'seizing' signal in '%s' state, sending 'delay-dialing'.\n", state_name);
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_DELAY_DIALING);
			/* change state */
			r1_new_state(r1, R1_STATE_IN_DELAY_DIALING);
			/* send proceed-to-send */
			set_tone(&r1->dsp, ' ', 0);
			/* make tone gerator transparent */
			set_tone_transparent(&r1->dsp, 1);
		}
		break;
	/* incoming call receives digits */
	case R1_STATE_RECV_DIGIT_WAIT:
		if (signal == 'B')
			goto clear_forward;
		if (!(signal >= '0' && signal <= '9') && !(signal >= 'a' && signal <= 'c')) {
			break;
		}
		LOGP_CHAN(DR1, LOGL_INFO, "Digit '%c' is detected in '%s' state.\n", signal, state_name);
		/* add digit */
		i = strlen(r1->dialing);
		if (i + 1 == sizeof(r1->dialing))
			break;
		r1->dialing[i++] = signal;
		r1->dialing[i] = '\0';
		/* change state */
		r1_new_state(r1, R1_STATE_RECV_DIGIT_ON);
		/* restart timer */
		osmo_timer_schedule(&r1->timer, TO_DIALING);
		break;
	case R1_STATE_RECV_DIGIT_ON:
		if (signal == 'B')
			goto clear_forward;
		if (signal != ' ')
			break;
		/* check for end of dialing */
		i = strlen(r1->dialing) - 1;
		if (r1->dialing[i] == 'c') {
			/* setup call */
			setup_call(r1);
			break;
		}
		/* change state */
		r1_new_state(r1, R1_STATE_RECV_DIGIT_WAIT);
		break;
	/* incoming call receives first pulse */
	case R1_STATE_RECV_PULSE_WAIT:
		if (signal == 'B') {
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_PULSE);
			/* change state */
			r1_new_state(r1, R1_STATE_RECV_PULSE_ON);
			/* start pulse counter */
			r1->pulse_counter = 1;
		}
		break;
	/* pulse goes off */
	case R1_STATE_RECV_PULSE_ON:
		if (signal == ' ') {
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_PULSE);
			/* change state */
			r1_new_state(r1, R1_STATE_RECV_PULSE_OFF);
		}
		break;
	/* pulse goes back on */
	case R1_STATE_RECV_PULSE_OFF:
		if (signal == 'B') {
			/* stop timer */
			osmo_timer_del(&r1->timer);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_PULSE);
			/* change state */
			r1_new_state(r1, R1_STATE_RECV_PULSE_ON);
			/* increment pulse counter */
			r1->pulse_counter++;
		}
		break;
	/* got a clear-forward */
	case R1_STATE_IN_PROCEED:
	case R1_STATE_IN_ANSWER:
	case R1_STATE_IN_HANGUP:
		if (signal == 'B') {
			clear_forward:
			LOGP_CHAN(DR1, LOGL_INFO, "Received 'clear-forward' signal in '%s' state'.\n", state_name);
			/* release outgoing call */
			if (r1->dsp.cc_callref) {
				/* send release indication towards CC */
				release_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
				/* remove ref */
				r1->dsp.cc_callref = 0;
			}
			/* stop SIT */
			set_sit(&r1->dsp, 0);
			/* Turn on SF */
			set_tone(&r1->dsp, 'B', 0);
			/* change state */
			r1_new_state(r1, R1_STATE_IDLE);
			/* reset inst */
			link_reset(r1);
		}
		break;
	default:
		LOGP_CHAN(DR1, LOGL_NOTICE, "Received signal '%c' in '%s' state is not handled!\n", signal, state_name);
	}
}

/* dialing was completed */
void dialing_complete(void *priv)
{
	r1_t *r1 = priv;

	LOGP_CHAN(DR1, LOGL_INFO, "Dialing is complete in '%s' state, waiting for remote party to answer.\n", r1_state_names[r1->state]);
	/* start timer */
	osmo_timer_schedule(&r1->timer, TO_PROCEEDING);
	/* change state */
	r1_new_state(r1, R1_STATE_OUT_PROCEED);
	/* indicate alerting now, because there is no alerting signaling */
	alert_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref);
}

/* timeouts */
static void r1_timeout(void *data)
{
	r1_t *r1 = data;
	const char *state_name = r1_state_names[r1->state];
	int i;

	switch (r1->state) {
	case R1_STATE_OUT_DELAY_DIALING:
		LOGP_CHAN(DR1, LOGL_INFO, "Detected 'double seizing', releasing call, going idle.\n");
		/* send 'idle' signal */
		set_tone(&r1->dsp, 'B', 0);
		/* state idle */
		r1_new_state(r1, R1_STATE_IDLE);
		/* send release indication towards CC */
		release_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NO_CIRCUIT_CHAN);
		/* reset inst */
		link_reset(r1);
		break;
	case R1_STATE_OUT_PROCEED:
		LOGP_CHAN(DR1, LOGL_INFO, "Received answer timeout, releasing call.\n");
		/* send clear-forward */
		set_tone(&r1->dsp, 'B', 0);
		/* change state */
		r1_new_state(r1, R1_STATE_IDLE);
		/* release call */
		release_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_USER_BUSY);
		/* reset inst */
		link_reset(r1);
		break;
	case R1_STATE_OUT_HANGUP:
		LOGP_CHAN(DR1, LOGL_INFO, "Received clear-back timeout, releasing call.\n");
		/* send clear-forward */
		set_tone(&r1->dsp, 'B', 0);
		/* change state */
		r1_new_state(r1, R1_STATE_IDLE);
		/* release call */
		release_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
		/* reset inst */
		link_reset(r1);
		break;
	case R1_STATE_IN_DELAY_DIALING:
		/* send 'proceed-to-send' signal */
		set_tone(&r1->dsp, 'B', 0);
		/* change state */
		if (r1->pulsedialing)
			r1_new_state(r1, R1_STATE_RECV_PULSE_WAIT);
		else {
			r1_new_state(r1, R1_STATE_RECV_DIGIT_WAIT);
			/* set recognition time */
			set_sig_detect_duration(&r1->dsp, SIGN_RECOGNITION_SLOW, 0.0);
		}
		/* start timer */
		osmo_timer_schedule(&r1->timer, TO_DIALING);
		break;
	case R1_STATE_RECV_DIGIT_WAIT:
	case R1_STATE_RECV_DIGIT_ON:
		LOGP_CHAN(DR1, LOGL_INFO, "Received dialing timeout in '%s' state, going to hangup state.\n", state_name);
		/* change state */
		r1_new_state(r1, R1_STATE_IN_HANGUP);
		/* send SIT */
		set_sit(&r1->dsp, 1);
		break;
	case R1_STATE_RECV_PULSE_WAIT:
		LOGP_CHAN(DR1, LOGL_INFO, "Received dialing timeout in '%s' state, setting up call.\n", state_name);
		/* setup call */
		setup_call(r1);
		break;
	case R1_STATE_RECV_PULSE_ON:
		LOGP_CHAN(DR1, LOGL_INFO, "Received pulse that was too long in '%s' state, assuming clear-forward.\n", state_name);
		/* Turn on SF */
		set_tone(&r1->dsp, 'B', 0);
		/* change state */
		r1_new_state(r1, R1_STATE_IDLE);
		/* reset inst */
		link_reset(r1);
		break;
	case R1_STATE_RECV_PULSE_OFF:
		/* start timer */
		osmo_timer_schedule(&r1->timer, TO_DIALING);
		/* add digit */
		i = strlen(r1->dialing);
		if (i + 1 == sizeof(r1->dialing))
			break;
		if (r1->pulse_counter <= 9)
			r1->dialing[i++] = '0' + r1->pulse_counter;
		else if (r1->pulse_counter == 10)
			r1->dialing[i++] = '0';
		else if (r1->pulse_counter == 11)
			r1->dialing[i++] = '*';
		else if (r1->pulse_counter == 12)
			r1->dialing[i++] = '#';
		else
			r1->dialing[i++] = '?';
		r1->dialing[i] = '\0';
		LOGP_CHAN(DR1, LOGL_INFO, "Received complete pulse sequence, storing digit %c.\n", r1->dialing[i - 1]);
		/* change state (after adding digit) */
		r1_new_state(r1, R1_STATE_RECV_PULSE_WAIT);
		break;
	case R1_STATE_IDLE:
		LOGP_CHAN(DR1, LOGL_INFO, "Received dialing timeout in '%s' state, guard timer has expired.\n", state_name);
		break;
	default:
		;
	}
}

/* message from call control */
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg)
{
	r1_endpoint_t *r1_ep = ep->priv;
	dsp_t *dsp;
	r1_t *r1;
	const char *state_name;
	osmo_cc_msg_t *new_msg;
	uint8_t type, plan, present, screen;
	char dialing[64];
	const char *sdp;
	int i;
	int rc;

	/* hunt for callref */
	dsp = r1_ep->dsp_list;
	while (dsp) {
		if (dsp->cc_callref == callref)
			break;
		dsp = dsp->next;
	}
	r1 = (dsp) ? dsp->priv : NULL;

	/* process SETUP */
	if (!r1) {
		if (msg->type != OSMO_CC_MSG_SETUP_REQ) {
			LOGP(DR1, LOGL_ERROR, "received message without r1 instance, please fix!\n");
			goto done;
		}
		/* hunt free r1 instance */
		dsp = r1_ep->dsp_list;
		while (dsp) {
			r1 = dsp->priv;
			/* Must be IDLE and guard timer expired. */
			if (r1->state == R1_STATE_IDLE && !osmo_timer_pending(&r1->timer))
				break;
			dsp = dsp->next;
		}
		r1 = (dsp) ? dsp->priv : NULL;
		if (!r1) {
			LOGP(DR1, LOGL_NOTICE, "No free r1 instance, rejecting.\n");
			reject_call(ep, callref, OSMO_CC_ISDN_CAUSE_NO_CIRCUIT_CHAN);
			goto done;
		}
		/* link with cc */
		r1->dsp.cc_callref = callref;
	}
	state_name = r1_state_names[r1->state];

	switch (msg->type) {
	case OSMO_CC_MSG_SETUP_REQ: /* dial-out command received from epoint */
		LOGP_CHAN(DR1, LOGL_INFO, "Outgoing call in '%s' state, sending 'seizing'.\n", state_name);
		/* caller id */
		rc = osmo_cc_get_ie_calling(msg, 0, &type, &plan, &present, &screen, r1->callerid, sizeof(r1->callerid));
		/* called number */
		rc = osmo_cc_get_ie_called(msg, 0, &type, &plan, dialing, sizeof(dialing));
		if (rc < 0 || !dialing[0]) {
			LOGP_CHAN(DR1, LOGL_NOTICE, "No number given, call is rejected!\n");
			inv_nr:
			/* reject call */
			reject_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_INV_NR_FORMAT);
			/* reset inst */
			link_reset(r1);
			goto done;
		}
		if (r1->pulsedialing) {
			if (strlen(dialing) + 1 > sizeof(r1->dialing)) {
				LOGP(DR1, LOGL_NOTICE, "Dial string is too long for our digit register, call is rejected!\n");
				goto inv_nr;
			}
			for (i = 0; dialing[i]; i++) {
				/* string must only consist of numerical digits and '*' (11 pulses) and '#' (12 pulses) */
				if (!strchr("0123456789*#", dialing[i])) {
					LOGP(DR1, LOGL_NOTICE, "Number has invalid digit '%c' at position %d, call is rejected!\n", dialing[i], i + 1);
					goto inv_nr;
				}
			}
			strcpy(r1->dialing, dialing);
		} else {
			rc = generate_dial_string(type, dialing, r1->dialing, sizeof(r1->dialing));
			if (rc < 0)
				goto inv_nr;
		}
		/* sdp accept */
		sdp = osmo_cc_helper_audio_accept(&r1->r1_ep->cc_ep.session_config, &r1->dsp, codecs + r1->r1_ep->no_l16, down_audio, msg, &r1->dsp.cc_session, &r1->dsp.codec, 0);
		if (!sdp) {
			/* reject call */
			reject_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			/* reset inst */
			link_reset(r1);
			goto done;
		}
		/* proceed */
		proceed_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, sdp);
		/* send seizing */
		set_tone(&r1->dsp, 0, 0);
		/* change state */
		r1_new_state(r1, R1_STATE_SEIZING);
		break;
	case OSMO_CC_MSG_SETUP_ACK_REQ: /* more information is needed */
	case OSMO_CC_MSG_PROC_REQ: /* call of endpoint is proceeding */
	case OSMO_CC_MSG_ALERT_REQ: /* call of endpoint is ringing */
	case OSMO_CC_MSG_PROGRESS_REQ: /* progress */
		rc = osmo_cc_helper_audio_negotiate(msg, &r1->dsp.cc_session, &r1->dsp.codec);
		if (rc < 0) {
			codec_failed:
			LOGP_CHAN(DR1, LOGL_NOTICE, "Releasing, because codec negotiation failed.\n");
			/* state hangup */
			r1_new_state(r1, R1_STATE_IN_HANGUP);
			/* send SIT */
			set_sit(&r1->dsp, 1);
			/* release call */
			release_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			/* reset inst */
			link_reset(r1);
			goto done;
		}
		break;
	case OSMO_CC_MSG_SETUP_RSP: /* call of endpoint is connected */
		LOGP_CHAN(DR1, LOGL_INFO, "Incoming call has answered in '%s' state, sending 'answer'.\n", state_name);
		rc = osmo_cc_helper_audio_negotiate(msg, &r1->dsp.cc_session, &r1->dsp.codec);
		if (rc < 0)
			goto codec_failed;
		/* connect acknowledge */
		setup_comp_call(&r1->r1_ep->cc_ep, r1->dsp.cc_callref);
		/* not in right state, which should never happen anyway */
		if (r1->state != R1_STATE_IN_PROCEED)
			break;
		/* send answer */
		set_tone(&r1->dsp, 0, 0);
		/* change state */
		r1_new_state(r1, R1_STATE_IN_ANSWER);
		break;
	case OSMO_CC_MSG_REJ_REQ: /* call has been rejected */
	case OSMO_CC_MSG_REL_REQ: /* call has been released */
	case OSMO_CC_MSG_DISC_REQ: /* call has been disconnected */
		rc = osmo_cc_helper_audio_negotiate(msg, &r1->dsp.cc_session, &r1->dsp.codec);
		if (rc < 0)
			goto codec_failed;
		/* right state */
		if (r1->state == R1_STATE_IN_PROCEED) {
			LOGP_CHAN(DR1, LOGL_INFO, "Incoming call has disconnected in '%s' state'.\n", state_name);
			/* state hangup */
			r1_new_state(r1, R1_STATE_IN_HANGUP);
			if (msg->type == OSMO_CC_MSG_REL_REQ) {
				/* send SIT */
				set_sit(&r1->dsp, 1);
			}
		} else
		if (r1->state == R1_STATE_IN_ANSWER) {
			LOGP_CHAN(DR1, LOGL_INFO, "Incoming call has disconnected in '%s' state, sending 'clear-back'.\n", state_name);
			/* send clear-back */
			set_tone(&r1->dsp, 'B', 0);
			/* state hangup */
			r1_new_state(r1, R1_STATE_IN_HANGUP);
			if (msg->type == OSMO_CC_MSG_REL_REQ) {
				/* send SIT */
				set_sit(&r1->dsp, 1);
			}
		} else {
			LOGP_CHAN(DR1, LOGL_INFO, "Outgoing call has disconnected in '%s' state, sending 'clear-forward'.\n", state_name);
			/* send clear-forward */
			set_tone(&r1->dsp, 'B', 0);
			/* start timer */
			osmo_timer_schedule(&r1->timer, TO_GUARD_BUSY);
			/* change state */
			r1_new_state(r1, R1_STATE_IDLE);
			if (msg->type == OSMO_CC_MSG_DISC_REQ) {
				/* clone osmo-cc message to preserve cause */
				new_msg = osmo_cc_clone_msg(msg);
				new_msg->type = OSMO_CC_MSG_REL_IND;
				/* send message to osmo-cc */
				osmo_cc_ll_msg(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, new_msg);
				/* reset */
				link_reset(r1);
				break;
			}
			// note: link_reset is called below
		}
		/* on release, we confirm */
		if (msg->type == OSMO_CC_MSG_REL_REQ) {
			/* clone osmo-cc message to preserve cause */
			new_msg = osmo_cc_clone_msg(msg);
			new_msg->type = OSMO_CC_MSG_REL_CNF;
			/* send message to osmo-cc */
			osmo_cc_ll_msg(&r1->r1_ep->cc_ep, r1->dsp.cc_callref, new_msg);
		}
		/* on reject/release we reset/unlink call */
		if (msg->type != OSMO_CC_MSG_DISC_REQ)
			link_reset(r1);
		break;
#if 0
	case OSMO_CC_MSG_INFO_REQ: /* we are getting overlap dialing */
		/* called number */
		rc = osmo_cc_get_ie_called(msg, 0, &type, &plan, dialing, sizeof(dialing));
		if (rc < 0 || !dialing[0]) {
			LOGP_CHAN(DR1, LOGL_NOTICE, "No number given, dialing ignored!\n");
			break;
		}
		if (!pulse) {
			LOGP_CHAN(DR1, LOGL_NOTICE, "Overlap dialing only supported with pulse dialing.\n");
			break;
		}
		/* concat overlap dialing to overlap register */
		strcat(r1->overlap, dialing, sizeof(r1->overlap) - 1);
		/* if inactive, dial the overlap string */
		if (r1->state == R1_STATE_OUT_INACTIVE) {
			/* dial */
			set_dial_string(&r1->dsp, r1->overlap, r1->pulsedialing);
			/* append the string to the dialed number */
			strcat(r1->dialing, r1->overlap, sizeof(r1->dialing) - 1);
			/* change state */
			r1_new_state(r1, R1_STATE_SEND_DIGITS);
		}
		break;
#endif
	}

done:
	osmo_cc_free_msg(msg);
}

