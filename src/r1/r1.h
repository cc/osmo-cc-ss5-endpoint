
#include "../common/dsp.h"

enum r1_state {
	R1_STATE_NULL = 0,
	/* idle line */
	R1_STATE_IDLE,			/* idle, also sending disconnect signal */
	/* outgoing call */
	R1_STATE_SEIZING,		/* sending seize, waiting for proceed-to-send */
	R1_STATE_OUT_DELAY_DIALING,	/* delay-dialing received, waiting for register to be available */
	R1_STATE_SEND_DIGITS,		/* proceed-to-send received, sending digits */
	R1_STATE_OUT_PROCEED,		/* waiting for called party to answer or being busy */
	R1_STATE_OUT_ANSWER,		/* detected answer */
	R1_STATE_OUT_HANGUP,		/* detected hangup */
	/* incoming call */
	R1_STATE_IN_DELAY_DIALING,	/* seize received, waiting for procced-to-send */
	R1_STATE_RECV_DIGIT_WAIT,	/* sending proceed-to-send, waiting for digits */
	R1_STATE_RECV_DIGIT_ON,		/* digit received, waiting to cease */
	R1_STATE_RECV_PULSE_WAIT,	/* sending proceed-to-send, waiting for first pulse of digit */
	R1_STATE_RECV_PULSE_ON,		/* pulse received, waiting to cease */
	R1_STATE_RECV_PULSE_OFF,	/* pulse ceased, waiting to for next pulse of digit */
	R1_STATE_IN_PROCEED,		/* waiting for called party to answer or disconnect */
	R1_STATE_IN_ANSWER,		/* called party answered */
	R1_STATE_IN_HANGUP,		/* called party disconnected, sending hangup */
};

extern const char *r1_state_names[];

struct r1_endpoint;

/* R1 link definition */
typedef struct r1 {
	struct r1_endpoint	*r1_ep;
	char			name[32];	/* name of link for debugging */

	/* call states */
	enum r1_state		state;		/* state of link */
	struct osmo_timer_list		timer;		/* for several timeouts */
	char			callerid[65];	/* current caller id (outgoing only) */
	char			dialing[33];	/* current dial string (send or receive) */
	int			pulsedialing;	/* if set, pulses are sent/received via SF */
	int			pulse_counter;	/* used to count received pulses */

	/* audio processing */
	dsp_t			dsp;		/* all dsp processing */
} r1_t;

/* R1 endpoint definition */
typedef struct r1_endpoint {
	osmo_cc_endpoint_t	cc_ep;
	dsp_t			*dsp_list;
	int			suppress_disconnect; /* do not forward disconnect towards CC */
	int			clear_back_timeout; /* time to disconnect when receiving clean back */
	int			no_l16; /* disable L16 codec */
} r1_endpoint_t;

void refresh_status(void);
r1_endpoint_t *r1_ep_create(const char *ep_name, int links, int suppress_disconnect, int clear_back_timeout, int crosstalk, int delay_ms, double sense_db, int pulsedialing, int no_l16);
void r1_ep_destroy(r1_endpoint_t *r1_ep);
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg);

