/* SS5 process
 *
 * (C) 2020 by Andreas Eversberg <jolly@eversberg.eu>
 * All Rights Reserved
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CHAN ss5->name

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <errno.h>
#include <sys/types.h>
#include "../liblogging/logging.h"
#include <osmocom/cc/g711.h>
#include "ss5.h"
#include "../common/display.h"
#include "../common/common.h"

/* names of all SS5 states */
const char *ss5_state_names[] = {
	"NULL",
	/* idle line */
	"IDLE",
	/* outgoing call */
	"SEND SEIZE",
	"RECV PROCEED-TO-SEND",
	"SEND DIGITS",
	"OUT INACTIVE",
	"SEND ACKNOWLEDGE (to answer)",
	"SEND ACKNOWLEDGE (to busy-flash)",
	"SEND ACKNOWLEDGE (to clear-back)",
	"OUT ACTIVE",
	"SEND CLEAR-FORWARD",
	"RECV RELEASE-GUARD",
	"SEND FORWARD-TRANSFER",
	/* incoming call */
	"SEND PROCEED-TO-SEND",
	"RECV DIGIT",
	"RECV SPACE",
	"IN INACTIVE",
	"SEND ANSWER",
	"IN ACTIVE",
	"SEND BUSY-FLASH",
	"SEND CLEAR-BACK",
	"SEND RELEASE-GUARD",
	"SEND RELEASE-GUARD (waiting)",
	/* seize collision */
	"DOUBLE-SEIZURE",
};

/* timers and durations */
#define INTERRUPT_RECOGNITION   0.015
#define SPLIT_RECOGNITION       0.030
#define MF_RECOGNITION          0.025
#define KP_DIGIT_DURATION	0.100
#define OTHER_DIGIT_DURATION	0.055
#define DIGIT_PAUSE		0.055
#define SIGN_RECOGNITION_FAST	0.040		/* 40 ms for seize and proceed-to-send */
#define SIGN_RECOGNITION_NORMAL	0.125		/* 125 ms for all other signals */
#define MIN_RELEASE_GUARD	0,200000	/* minimum 200 ms, in case we prevent blueboxing */
#define MAX_SEIZE		10.0
#define MAX_PROCEED_TO_SEND	4.0
#define MAX_ANSWER		10.0
#define MAX_BUSY_FLASH		10.0
#define MAX_CLEAR_BACK		10.0
#define MAX_ACKNOWLEDGE		4.0
#define MAX_CLEAR_FORWARD	10.0
#define MAX_RELEASE_GUARD	4.0
#define DUR_DOUBLE_SEIZURE	0,850000	/* 850 ms to be sure the other end recognizes the double seizure */
#define DUR_FORWARD_TRANSFER	0.850		/* 850 ms forward-transfer */
#define PAUSE_BEFORE_DIALING	0.080		/* pause before dialing after cease of tone */
#define TO_DIALING		10,0		/* as defined in clause about releasing the incoming register when number is incomplete */

static struct osmo_cc_helper_audio_codecs codecs[] = {
	{ "L16", 8000, 1, encode_l16, decode_l16 },
	{ "PCMA", 8000, 1, g711_encode_alaw, g711_decode_alaw },
	{ "PCMU", 8000, 1, g711_encode_ulaw, g711_decode_ulaw },
	{ NULL, 0, 0, NULL, NULL},
};

void refresh_status(void)
{
	osmo_cc_endpoint_t *ep;
	ss5_endpoint_t *ss5_ep;
	dsp_t *dsp;
	ss5_t *ss5;
	int i;

	display_status_start();

	for (ep = osmo_cc_endpoint_list; ep; ep = ep->next) {
		ss5_ep = ep->priv;
		if (!ss5_ep->dsp_list)
			display_status_line(ep->local_name, 0, NULL, NULL, "");
		for (i = 0, dsp = ss5_ep->dsp_list; dsp; i++, dsp = dsp->next) {
			ss5 = dsp->priv;
			display_status_line(ep->local_name, i, ss5->callerid, ss5->dialing, ss5_state_names[ss5->state]);
		}
	}

	display_status_end();
}

static void ss5_new_state(ss5_t *ss5, enum ss5_state state)
{
	LOGP_CHAN(DSS5, LOGL_DEBUG, "Changing state  '%s' -> '%s'\n", ss5_state_names[ss5->state], ss5_state_names[state]);
	ss5->state = state;

	/* must update (new state) */
	refresh_status();
}

/*
 * endpoints & links
 */

/* reset ss5 link, but keep states, so audio generation/processing can continue */
static void link_reset(ss5_t *ss5)
{
	/* unlink callref */
	ss5->dsp.cc_callref = 0;

	/* stop timer */
	osmo_timer_del(&ss5->timer);

	/* free session description */
	if (ss5->dsp.cc_session) {
		osmo_cc_free_session(ss5->dsp.cc_session);
		ss5->dsp.cc_session = NULL;
		ss5->dsp.codec = NULL;
	}

	/* reset jitter buffer */
	jitter_reset(&ss5->dsp.tx_dejitter);

	/* set recognition time */
	set_sig_detect_duration(&ss5->dsp, SIGN_RECOGNITION_FAST, SIGN_RECOGNITION_NORMAL);

	/* reset all other states */
	ss5->callerid[0] = '\0';
	ss5->dialing[0] = '\0';

	/* must update (e.g. caller and dialing) */
	refresh_status();
}

static void ss5_timeout(void *data);

static ss5_t *link_create(ss5_endpoint_t *ss5_ep, const char *ep_name, int linkid, double samplerate, int crosstalk, int comfort_noise, int delay_ms, double sense_db)
{
	ss5_t *ss5;
	dsp_t **dsp_p;

	ss5 = calloc(1, sizeof(*ss5));
	if (!ss5) {
		LOGP(DSS5, LOGL_ERROR, "No memory!\n");
		abort();
	}
	ss5->ss5_ep = ss5_ep;

	/* debug name */
	snprintf(ss5->name, sizeof(ss5->name) - 1, "%s/%d", ep_name, linkid);

	/* init dsp instance */
	dsp_init_inst(&ss5->dsp, ss5, ss5->name, samplerate, crosstalk, comfort_noise, delay_ms, sense_db, INTERRUPT_RECOGNITION, SPLIT_RECOGNITION, MF_RECOGNITION, KP_DIGIT_DURATION, OTHER_DIGIT_DURATION, DIGIT_PAUSE, 0.0, 0.0, 1);

	/* init timer */
	osmo_timer_setup(&ss5->timer, ss5_timeout, ss5);

	/* reset instance */
	link_reset(ss5);

	/* state idle */
	ss5_new_state(ss5, SS5_STATE_IDLE);

	/* link */
	dsp_p = &ss5_ep->dsp_list;
	while (*dsp_p)
		dsp_p = &((*dsp_p)->next);
	*dsp_p = &ss5->dsp;

	LOGP_CHAN(DSS5, LOGL_DEBUG, "created ss5 instance\n");

	return ss5;
}

static void link_destroy(ss5_t *ss5)
{
	dsp_t **dsp_p;

	/* detach */
	dsp_p = &ss5->ss5_ep->dsp_list;
	while (*dsp_p) {
		if (*dsp_p == &ss5->dsp)
			break;
		dsp_p = &((*dsp_p)->next);
	}
	*dsp_p = ss5->dsp.next;

	/* state idle */
	ss5_new_state(ss5, SS5_STATE_NULL);

	/* reset instance */
	link_reset(ss5);

	/* exit timer */
	osmo_timer_del(&ss5->timer);

	/* cleanup dsp instance */
	dsp_cleanup_inst(&ss5->dsp);

	LOGP_CHAN(DSS5, LOGL_DEBUG, "destroyed ss5 instance\n");

	free(ss5);
}

ss5_endpoint_t *ss5_ep_create(const char *ep_name, int links, int prevent_blueboxing, int suppress_disconnect, int crosstalk, int comfort_noise, int delay_ms, double sense_db, int no_l16)
{
	ss5_endpoint_t *ss5_ep;
	int i;

	ss5_ep = calloc(1, sizeof(*ss5_ep));
	if (!ss5_ep) {
		LOGP(DSS5, LOGL_ERROR, "No memory!\n");
		abort();
	}

	ss5_ep->prevent_blueboxing = prevent_blueboxing;
	ss5_ep->suppress_disconnect = suppress_disconnect;
	ss5_ep->no_l16 = !!no_l16;

	for (i = 0; i < links; i++)
		link_create(ss5_ep, ep_name, i + 1, 8000.0, crosstalk, comfort_noise, delay_ms, sense_db);

	LOGP(DSS5, LOGL_DEBUG, "SS5 endpoint instance created\n");

	return ss5_ep;
}

void ss5_ep_destroy(ss5_endpoint_t *ss5_ep)
{
	/* destroy all calls */
	while (ss5_ep->dsp_list)
		link_destroy(ss5_ep->dsp_list->priv);

	free(ss5_ep);

	LOGP(DSS5, LOGL_DEBUG, "SS5 endpoint instance destroyed\n");
}

/*
 * event handling
 */

/* function that receives the signal or the cease of it (' ' or different signal) */
void receive_signal(void *priv, char signal, double dbm)
{
	ss5_t *ss5 = priv;
	const char *state_name = ss5_state_names[ss5->state];
	int i;
	int rc;

	if (signal > ' ')
		LOGP_CHAN(DSS5, LOGL_DEBUG, "Received signal '%c' in '%s' state. (%.1f dBm)\n", signal, state_name, dbm);
	else
		LOGP_CHAN(DSS5, LOGL_DEBUG, "Received cease of signal in '%s' state.\n", state_name);

	/* a clear forward (not release guard) at any state (including idle state) */
	if (ss5->state != SS5_STATE_SEND_CLR_FWD && signal == 'C') {
		LOGP_CHAN(DSS5, LOGL_INFO, "Received 'clear-forward' signal in '%s' state, sending 'release-guard' and clearing call.\n", state_name);
		/* release outgoing call */
		if (ss5->dsp.cc_callref) {
			/* send release indication towards CC */
			release_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
			/* remove ref */
			ss5->dsp.cc_callref = 0;
		}
		/* stop timer */
		osmo_timer_del(&ss5->timer);
		/* change state */
		ss5_new_state(ss5, SS5_STATE_SEND_RELEASE);
		/* unset dial string, if set */
		set_dial_string(&ss5->dsp, "", 0);
		/* send release-guard */
		set_tone(&ss5->dsp, 'C', 0);
		/* to prevent blueboxing */
		if (ss5->ss5_ep->prevent_blueboxing) {
			LOGP_CHAN(DSS5, LOGL_INFO, "Starting release-guard timer to prevent blueboxing.\n");
			/* start timer */
			osmo_timer_schedule(&ss5->timer, MIN_RELEASE_GUARD);
		}
		return;
	}

	switch (ss5->state) {
	/* release guard */
	case SS5_STATE_SEND_RELEASE:
		if (signal != 'C') {
			/* wait at least the minimum release-guard time, to prevent blueboxing */
			if (osmo_timer_pending(&ss5->timer)) {
				LOGP_CHAN(DSS5, LOGL_INFO, "'clear-forward' is ceased in '%s' state, must wait to prevent blueboxing.\n", state_name);
				/* state idle */
				ss5_new_state(ss5, SS5_STATE_SEND_REL_WAIT);
				break;
			}
			LOGP_CHAN(DSS5, LOGL_INFO, "'clear-forward' is ceased in '%s' state, going idle.\n", state_name);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* state idle */
			ss5_new_state(ss5, SS5_STATE_IDLE);
			/* reset instance */
			link_reset(ss5);
		}
		break;
	/* outgoing call sends seize */
	case SS5_STATE_SEND_SEIZE:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'seize' signal in '%s' state, this is double seizure.\n", state_name);
			/* set timeout */
			osmo_timer_schedule(&ss5->timer, DUR_DOUBLE_SEIZURE);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_DOUBLE_SEIZE);
		}
		if (signal == 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'proceed-to-send' signal in '%s' state, ceasing 'seize' signal.\n", state_name);
			/* set recognition time to normal */
			set_sig_detect_duration(&ss5->dsp, SIGN_RECOGNITION_NORMAL, SIGN_RECOGNITION_NORMAL);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_RECV_PROCEED);
		}
		break;
	/* both ends send a seize, waiting for timeout */
	case SS5_STATE_DOUBLE_SEIZE:
		break;
	/* outgoing call receives proceed-to-send */
	case SS5_STATE_RECV_PROCEED:
		if (signal != 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "proceed-to-send' is ceased in '%s' state, sendig digits.\n", state_name);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* dial */
			set_tone(&ss5->dsp, ' ', PAUSE_BEFORE_DIALING);
			set_dial_string(&ss5->dsp, ss5->dialing, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_DIGITS);
		}
		break;
	/* outgoing call receives answer or busy-flash */
	case SS5_STATE_OUT_INACTIVE:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'answer' signal in '%s' state, sending 'acknowledge'.\n", state_name);
			/* send acknowledge */
			set_tone(&ss5->dsp, 'A', MAX_ACKNOWLEDGE);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_ACK_ANS);
			/* indicate answer to upper layer */
			answer_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref);
		}
		if (signal == 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'busy-flash' signal in '%s' state, sending 'acknowledge'.\n", state_name);
			/* send acknowledge */
			set_tone(&ss5->dsp, 'A', MAX_ACKNOWLEDGE);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_ACK_BUS);
			if (!ss5->ss5_ep->suppress_disconnect) {
				/* indicate disconnect w/tones to upper layer */
				disconnect_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_USER_BUSY);
			}
		}
		break;
	/* outgoing call receives clear-back */
	case SS5_STATE_OUT_ACTIVE:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'answer' signal in '%s' state, sending 'acknowledge'.\n", state_name);
			/* send acknowledge */
			set_tone(&ss5->dsp, 'A', MAX_ACKNOWLEDGE);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_ACK_ANS);
			/* indicate answer to upper layer */
			answer_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref);
		}
		if (signal == 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'clear-back' signal in '%s' state, sending 'acknowledge'.\n", state_name);
			/* send acknowledge */
			set_tone(&ss5->dsp, 'A', MAX_ACKNOWLEDGE);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_ACK_CLR);
			if (!ss5->ss5_ep->suppress_disconnect) {
				/* indicate disconnect w/tones to upper layer */
				disconnect_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
			}
		}
		break;
	/* outgoing call receives answer */
	case SS5_STATE_SEND_ACK_ANS:
		if (signal != 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "'answer' is ceased in '%s' state, call is established.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_OUT_ACTIVE);
		}
		break;
	/* outgoing call receives busy-flash */
	case SS5_STATE_SEND_ACK_BUS:
	case SS5_STATE_SEND_DIGITS:
		if (signal != 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "'busy-flash' is ceased in '%s' state, call is disconnected.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* unset dial string, if set */
			set_dial_string(&ss5->dsp, "", 0);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_OUT_INACTIVE);
		}
		break;
	/* outgoing call receives clear-back */
	case SS5_STATE_SEND_ACK_CLR:
		if (signal != 'B') {
			LOGP_CHAN(DSS5, LOGL_INFO, "'clear-back' is ceased in '%s' state, call is disconnected.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_OUT_INACTIVE);
		}
		break;
	/* outgoing call sends clear forward */
	case SS5_STATE_SEND_CLR_FWD:
		if (signal == 'C') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'release-guard' signal in '%s' state, ceasing 'clear-forward' signal.\n", state_name);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_RECV_RELEASE);
		}
		break;
	/* outgoing call receives release guard */
	case SS5_STATE_RECV_RELEASE:
		if (signal != 'C') {
			LOGP_CHAN(DSS5, LOGL_INFO, "'release-guard' is ceased in '%s' state, going idle.\n", state_name);
			/* state idle */
			ss5_new_state(ss5, SS5_STATE_IDLE);
			/* reset instance */
			link_reset(ss5);
		}
		break;
	/* incoming call receives seize */
	case SS5_STATE_IDLE:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'seize' signal in '%s' state, sending 'proceed-to-send'.\n", state_name);
			/* set recognition time to normal */
			set_sig_detect_duration(&ss5->dsp, SIGN_RECOGNITION_NORMAL, SIGN_RECOGNITION_NORMAL);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_PROCEED);
			/* send proceed-to-send */
			set_tone(&ss5->dsp, 'B', MAX_PROCEED_TO_SEND);
		}
		break;
	/* incoming call sends proceed-to-send */
	case SS5_STATE_SEND_PROCEED:
		if (signal != 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "'seize' is ceased in '%s' state, receiving digits.\n", state_name);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_RECV_DIGIT);
			/* start timer */
			osmo_timer_schedule(&ss5->timer, TO_DIALING);
		}
		break;
	/* incoming call receives digits */
	case SS5_STATE_RECV_DIGIT:
		if (!(signal >= '0' && signal <= '9') && !(signal >= 'a' && signal <= 'c')) {
			break;
		}
		LOGP_CHAN(DSS5, LOGL_INFO, "Digit '%c' is detected in '%s' state.\n", signal, state_name);
		/* add digit */
		i = strlen(ss5->dialing);
		if (i + 1 == sizeof(ss5->dialing))
			break;
		ss5->dialing[i++] = signal;
		ss5->dialing[i] = '\0';
		/* change state */
		ss5_new_state(ss5, SS5_STATE_RECV_SPACE);
		/* restart timer */
		osmo_timer_schedule(&ss5->timer, TO_DIALING);
		break;
	case SS5_STATE_RECV_SPACE:
		if (signal != ' ')
			break;
		/* check for end of dialing */
		i = strlen(ss5->dialing) - 1;
		if (ss5->dialing[i] == 'c') {
			osmo_cc_msg_t *msg;
			uint8_t type;
			char dialing[sizeof(ss5->dialing)];
			LOGP_CHAN(DSS5, LOGL_INFO, "Dialing '%s' is complete, sending setup message towards call control.\n", ss5->dialing);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* check dial string */
			rc = parse_dial_string(&type, dialing, sizeof(dialing), ss5->dialing);
			if (rc < 0) {
				/* send clear-back */
				set_tone(&ss5->dsp, 'B', MAX_CLEAR_BACK);
				/* change state */
				ss5_new_state(ss5, SS5_STATE_SEND_CLR_BAK);
				break;
			}
			/* setup message */
			msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_IND);
			/* network type */
			osmo_cc_add_ie_calling_network(msg, OSMO_CC_NETWORK_SS5_NONE, "");
			/* called number */
			osmo_cc_add_ie_called(msg, type, OSMO_CC_PLAN_TELEPHONY, dialing);
			/* sdp offer */
			ss5->dsp.cc_session = osmo_cc_helper_audio_offer(&ss5->ss5_ep->cc_ep.session_config, &ss5->dsp, codecs + ss5->ss5_ep->no_l16, down_audio, msg, 1);
			if (!ss5->dsp.cc_session) {
				osmo_cc_free_msg(msg);
				LOGP_CHAN(DSS5, LOGL_NOTICE, "Failed to offer audio, sending 'clear-back'.\n");
				/* send clear-back */
				set_tone(&ss5->dsp, 'B', MAX_CLEAR_BACK);
				/* change state */
				ss5_new_state(ss5, SS5_STATE_SEND_CLR_BAK);
				break;
			}
			/* create new call */
			osmo_cc_call_t *cc_call = osmo_cc_call_new(&ss5->ss5_ep->cc_ep);
			ss5->dsp.cc_callref = cc_call->callref;
			/* send message to CC */
			osmo_cc_ll_msg(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, msg);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_IN_INACTIVE);
			break;
		}
		/* change state */
		ss5_new_state(ss5, SS5_STATE_RECV_DIGIT);
		break;
	/* incoming call sends answer */
	case SS5_STATE_SEND_ANSWER:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'acknowledge' to 'answer' in '%s' state, call is now active.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_IN_ACTIVE);
		}
		break;
	/* incoming call sends busy-flash */
	case SS5_STATE_SEND_BUSY:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'acknowledge' to 'busy-flash' in '%s' state, call is now inactive.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_IN_INACTIVE);
		}
		break;
	/* incoming call sends clear-back */
	case SS5_STATE_SEND_CLR_BAK:
		if (signal == 'A') {
			LOGP_CHAN(DSS5, LOGL_INFO, "Received 'acknowledge' to 'clear-back' in '%s' state, call is now inactive.\n", state_name);
			/* stop timer */
			osmo_timer_del(&ss5->timer);
			/* cease */
			set_tone(&ss5->dsp, 0, 0);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_IN_INACTIVE);
		}
		break;
	default:
		LOGP_CHAN(DSS5, LOGL_NOTICE, "Received signal '%c' in '%s' state is not handled!\n", signal, state_name);
	}
}

/* dialing was completed */
void dialing_complete(void *priv)
{
	ss5_t *ss5 = priv;

	if (ss5->state == SS5_STATE_SEND_DIGITS) {
		LOGP_CHAN(DSS5, LOGL_INFO, "Dialing is complete in '%s' state, waiting for remote party to answer.\n", ss5_state_names[ss5->state]);
		/* change state */
		ss5_new_state(ss5, SS5_STATE_OUT_INACTIVE);
	}

	/* indicate alerting */
	alert_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref);
}

/* timeouts */
static void ss5_timeout(void *data)
{
	ss5_t *ss5 = data;
	const char *state_name = ss5_state_names[ss5->state];

	switch (ss5->state) {
	case SS5_STATE_RECV_DIGIT:
	case SS5_STATE_RECV_SPACE:
		LOGP_CHAN(DSS5, LOGL_INFO, "Received timeout in '%s' state, sending 'clear-back'.\n", state_name);
		/* send clear-back */
		set_tone(&ss5->dsp, 'B', MAX_CLEAR_BACK);
		/* change state */
		ss5_new_state(ss5, SS5_STATE_SEND_CLR_BAK);
		break;
	case SS5_STATE_DOUBLE_SEIZE:
		/* cease */
		set_tone(&ss5->dsp, 0, 0);
		/* state idle */
		ss5_new_state(ss5, SS5_STATE_IDLE);
		/* send release indication towards CC */
		release_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_NO_CIRCUIT_CHAN);
		/* reset inst */
		link_reset(ss5);
		break;
	case SS5_STATE_SEND_REL_WAIT:
		LOGP_CHAN(DSS5, LOGL_INFO, "'release-guard' timer expired, going idle.\n");
		/* cease */
		set_tone(&ss5->dsp, 0, 0);
		/* state idle */
		ss5_new_state(ss5, SS5_STATE_IDLE);
		/* reset instance */
		link_reset(ss5);
		break;
	default:
		;
	}
}

/* message from call control */
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg)
{
	ss5_endpoint_t *ss5_ep = ep->priv;
	dsp_t *dsp;
	ss5_t *ss5;
	const char *state_name;
	osmo_cc_msg_t *new_msg;
	uint8_t type, plan, present, screen;
	char dialing[64];
	const char *sdp;
	int rc;

	/* hunt for callref */
	dsp = ss5_ep->dsp_list;
	while (dsp) {
		if (dsp->cc_callref == callref)
			break;
		dsp = dsp->next;
	}
	ss5 = (dsp) ? dsp->priv : NULL;

	/* process SETUP */
	if (!ss5) {
		if (msg->type != OSMO_CC_MSG_SETUP_REQ) {
			LOGP(DSS5, LOGL_ERROR, "received message without ss5 instance, please fix!\n");
			goto done;
		}
		/* hunt free ss5 instance */
		dsp = ss5_ep->dsp_list;
		while (dsp) {
			ss5 = dsp->priv;
			if (ss5->state == SS5_STATE_IDLE)
				break;
			dsp = dsp->next;
		}
		ss5 = (dsp) ? dsp->priv : NULL;
		if (!ss5) {
			LOGP(DSS5, LOGL_NOTICE, "No free ss5 instance, rejecting.\n");
			reject_call(&ss5_ep->cc_ep, callref, OSMO_CC_ISDN_CAUSE_NO_CIRCUIT_CHAN);
			goto done;
		}
		/* link with cc */
		ss5->dsp.cc_callref = callref;
	}
	state_name = ss5_state_names[ss5->state];

	switch (msg->type) {
	case OSMO_CC_MSG_SETUP_REQ: /* dial-out command received from epoint */
		LOGP_CHAN(DSS5, LOGL_INFO, "Outgoing call in '%s' state, sending 'seize'.\n", state_name);
		/* caller id */
		rc = osmo_cc_get_ie_calling(msg, 0, &type, &plan, &present, &screen, ss5->callerid, sizeof(ss5->callerid));
		/* called number */
		rc = osmo_cc_get_ie_called(msg, 0, &type, &plan, dialing, sizeof(dialing));
		if (rc < 0 || !dialing[0]) {
			LOGP_CHAN(DSS5, LOGL_NOTICE, "No number given, call is rejected!\n");
			inv_nr:
			reject_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_INV_NR_FORMAT);
			link_reset(ss5);
			goto done;
		}
		rc = generate_dial_string(type, dialing, ss5->dialing, sizeof(ss5->dialing));
		if (rc < 0)
			goto inv_nr;
		/* sdp accept */
		sdp = osmo_cc_helper_audio_accept(&ss5->ss5_ep->cc_ep.session_config, &ss5->dsp, codecs + ss5->ss5_ep->no_l16, down_audio, msg, &ss5->dsp.cc_session, &ss5->dsp.codec, 0);
		if (!sdp) {
			reject_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			link_reset(ss5);
			goto done;
		}
		/* proceed */
		proceed_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, sdp);
		/* send seize */
		set_tone(&ss5->dsp, 'A', MAX_SEIZE);
		/* change state */
		ss5_new_state(ss5, SS5_STATE_SEND_SEIZE);
		break;
	case OSMO_CC_MSG_SETUP_ACK_REQ: /* more information is needed */
	case OSMO_CC_MSG_PROC_REQ: /* call of endpoint is proceeding */
	case OSMO_CC_MSG_ALERT_REQ: /* call of endpoint is ringing */
	case OSMO_CC_MSG_PROGRESS_REQ: /* progress */
		rc = osmo_cc_helper_audio_negotiate(msg, &ss5->dsp.cc_session, &ss5->dsp.codec);
		if (rc < 0) {
			codec_failed:
			LOGP_CHAN(DSS5, LOGL_NOTICE, "Releasing, because codec negotiation failed.\n");
			/* send busy-flash */
			set_tone(&ss5->dsp, 'B', MAX_BUSY_FLASH);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_BUSY);
			/* release call */
			release_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			/* reset inst */
			link_reset(ss5);
			goto done;
		}
		break;
	case OSMO_CC_MSG_SETUP_RSP: /* call of endpoint is connected */
		LOGP_CHAN(DSS5, LOGL_INFO, "Incoming call has answered in '%s' state, sending 'answer'.\n", state_name);
		rc = osmo_cc_helper_audio_negotiate(msg, &ss5->dsp.cc_session, &ss5->dsp.codec);
		if (rc < 0)
			goto codec_failed;
		/* connect acknowledge */
		setup_comp_call(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref);
		/* not in right state, which should never happen anyway */
		if (ss5->state != SS5_STATE_IN_INACTIVE)
			break;
		/* send answer */
		set_tone(&ss5->dsp, 'A', MAX_ANSWER);
		/* change state */
		ss5_new_state(ss5, SS5_STATE_SEND_ANSWER);
		break;
	case OSMO_CC_MSG_REJ_REQ: /* call has been rejected */
	case OSMO_CC_MSG_REL_REQ: /* call has been released */
	case OSMO_CC_MSG_DISC_REQ: /* call has been disconnected */
		rc = osmo_cc_helper_audio_negotiate(msg, &ss5->dsp.cc_session, &ss5->dsp.codec);
		if (rc < 0)
			goto codec_failed;
		/* right state */
		if (ss5->state == SS5_STATE_IN_INACTIVE) {
			LOGP_CHAN(DSS5, LOGL_INFO, "Incoming call has disconnected in '%s' state, sending 'busy-flash'.\n", state_name);
			/* send busy-flash */
			set_tone(&ss5->dsp, 'B', MAX_BUSY_FLASH);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_BUSY);
		} else
		if (ss5->state == SS5_STATE_IN_ACTIVE) {
			LOGP_CHAN(DSS5, LOGL_INFO, "Incoming call has disconnected in '%s' state, sending 'clear-back'.\n", state_name);
			/* send clear-back */
			set_tone(&ss5->dsp, 'B', MAX_CLEAR_BACK);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_CLR_BAK);
		} else {
			LOGP_CHAN(DSS5, LOGL_INFO, "Outgoing call has disconnected in '%s' state, sending 'clear-forward'.\n", state_name);
			/* send clear-forward */
			set_tone(&ss5->dsp, 'C', MAX_CLEAR_FORWARD);
			/* change state */
			ss5_new_state(ss5, SS5_STATE_SEND_CLR_FWD);
			if (msg->type == OSMO_CC_MSG_DISC_REQ) {
				/* clone osmo-cc message to preserve cause */
				new_msg = osmo_cc_clone_msg(msg);
				new_msg->type = OSMO_CC_MSG_REL_IND;
				/* send message to osmo-cc */
				osmo_cc_ll_msg(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, new_msg);
				/* reset */
				link_reset(ss5);
				break;
			}
		}
		/* on release, we confirm */
		if (msg->type == OSMO_CC_MSG_REL_REQ) {
			/* clone osmo-cc message to preserve cause */
			new_msg = osmo_cc_clone_msg(msg);
			new_msg->type = OSMO_CC_MSG_REL_CNF;
			/* send message to osmo-cc */
			osmo_cc_ll_msg(&ss5->ss5_ep->cc_ep, ss5->dsp.cc_callref, new_msg);
		}
		/* on reject/release we reset/unlink call */
		if (msg->type != OSMO_CC_MSG_DISC_REQ)
			link_reset(ss5);
		break;
	}

done:
	osmo_cc_free_msg(msg);
}

