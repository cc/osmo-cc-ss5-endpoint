
#include "../common/dsp.h"

enum ss5_state {
	SS5_STATE_NULL = 0,
	/* idle line */
	SS5_STATE_IDLE,
	/* outgoing call */
	SS5_STATE_SEND_SEIZE,	/* sending seize, waiting for proceed-to-send */
	SS5_STATE_RECV_PROCEED,	/* detected proceed-to-send, waiting to cease */
	SS5_STATE_SEND_DIGITS,	/* proceed-to-send is ceased, sending digits */
	SS5_STATE_OUT_INACTIVE,	/* waiting for called party to answer or being busy */
	SS5_STATE_SEND_ACK_ANS,	/* detected answer, sending acknowledge, waiting to cease */
	SS5_STATE_SEND_ACK_BUS,	/* detected busy-flash, sending acknowledge, waiting to cease */
	SS5_STATE_SEND_ACK_CLR,	/* detected clear-back, sending acknowledge, waiting to cease */
	SS5_STATE_OUT_ACTIVE,	/* detected answer is ceased */
	SS5_STATE_SEND_CLR_FWD,	/* sending clear-forward, waiting for release-guard */
	SS5_STATE_RECV_RELEASE,	/* receiving release-guard, waiting to cease */
	SS5_STATE_SEND_FORWARD,	/* sending forward-transfer for a while */
	/* incoming call */
	SS5_STATE_SEND_PROCEED,	/* detected seize, sending procced-to-send, waiting to cease */
	SS5_STATE_RECV_DIGIT,	/* seize is ceased, waiting for digits */
	SS5_STATE_RECV_SPACE,	/* digit received, waiting to cease */
	SS5_STATE_IN_INACTIVE,	/* waiting for called party to answer or disconnect */
	SS5_STATE_SEND_ANSWER,	/* sending answer, waiting for for acknowledge */
	SS5_STATE_IN_ACTIVE,	/* detected acknowledge of answer */
	SS5_STATE_SEND_BUSY,	/* sending busy-flash, waiting for acknowledge */
	SS5_STATE_SEND_CLR_BAK,	/* sending clear back (hangup) */
	SS5_STATE_SEND_RELEASE,	/* detected clear-forward, sending release-guard, waiting for cease */
	SS5_STATE_SEND_REL_WAIT,/* clear forward ceased, but waiting to prevent blueboxing */
	/* seize collision */
	SS5_STATE_DOUBLE_SEIZE,	/* seize is detected, sending for while, waiting for cease */
};

extern const char *ss5_state_names[];

struct ss5_endpoint;

/* SS5 link definition */
typedef struct ss5 {
	struct ss5_endpoint	*ss5_ep;
	char			name[32];	/* name of link for debugging */

	/* call states */
	enum ss5_state		state;		/* state of link */
	struct osmo_timer_list		timer;		/* for several timeouts */
	char			callerid[65];	/* current caller id (outgoing only) */
	char			dialing[33];	/* current dial string (send or receive) */

	/* audio processing */
	dsp_t			dsp;		/* all dsp processing */
} ss5_t;

/* SS5 endpoint definition */
typedef struct ss5_endpoint {
	osmo_cc_endpoint_t	cc_ep;
	dsp_t			*dsp_list;
	int			suppress_disconnect; /* do not forward disconnect towards CC */
	int			prevent_blueboxing; /* extend release-guard, so outgoing exchange releases */
	int			no_l16; /* disable L16 codec */
} ss5_endpoint_t;

void refresh_status(void);
ss5_endpoint_t *ss5_ep_create(const char *ep_name, int links, int prevent_blueboxing, int suppress_disconnect, int crosstalk, int comfort_noise, int delay_ms, double sense_db, int no_l16);
void ss5_ep_destroy(ss5_endpoint_t *ss5_ep);
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg);

